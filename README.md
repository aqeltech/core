# Changelog
# v1.2.0
# Migrated Legacy Setup to Declarative Scheme

# v1.0.3
# Added NoIndex/NoFollow to Filtered Categories to improve the crawlability.